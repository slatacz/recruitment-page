(()=>{
    const validateForm = ()=>{
        let valid = {
            valid:true,
            name: true,
            surname: true,
            phone: true,
            service: true
        };
        const namev = document.querySelector('input[name="name"]').value;
        const surnamev = document.querySelector('input[name="surname"]').value;
        const phonev = document.querySelector('input[name="phone"]').value;
        const servicev = document.querySelector('select[name="service"]').value;
        const regex = /^\+?[0-9]{3}-?[0-9]{6,12}$/;
        (namev && namev !== '') ? '': (valid.valid=false, valid.name=false);
        (surnamev && surnamev !== '') ? '': (valid.valid=false,valid.surname=false);
        (phonev && phonev !== '' && regex.test(phonev))? '':(valid.valid=false,valid.phone=false);
        (servicev && servicev !== 'Select Service') ? '': (valid.valid=false,valid.service=false);
        return valid;
    }
    document.querySelector('#opener').addEventListener('click', ()=>{
        document.querySelector('nav').classList.toggle('opened');
    });
    let header = document.querySelector('header').offsetHeight;
    window.addEventListener("scroll", ()=>{
        if((document.documentElement || document.body.parentNode || document.body).scrollTop>header){
            !document.querySelector('nav').classList.contains('alternative') ? document.querySelector('nav').classList.add('alternative'): '';
        }else{
            document.querySelector('nav').classList.contains('alternative') ? document.querySelector('nav').classList.remove('alternative'): '';
        }
    });
    document.querySelector('form').addEventListener('submit', (e)=>{
        e.preventDefault();
        const valid = validateForm();
        if(!valid.valid) window.alert(`Enter correct value of:  ${valid.name?'': '\n name '}${valid.surname?'': '\n surname '}${valid.phone?'': '\n phone number '}${valid.service?'': '\n service '}`)
        else{
            var xhr = new XMLHttpRequest();
            xhr.open("POST", 'apiAdress.com/endpoint', true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify({
                name: document.querySelector('input[name="name"]').value,
                surname: document.querySelector('input[name="surname"]').value,
                phone: document.querySelector('input[name="phone"]').value,
                service: document.querySelector('select[name="service"]').value
            }));
        }
    });
 })();